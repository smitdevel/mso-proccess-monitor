﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace MSOProccessMonitor
{
    public partial class MSOProcessMonitor : Form
    {

        BackgroundWorker m_oWorker;

        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MSOProcessMonitor()
        {

            log.Info("Initialize form components.");
            InitializeComponent();

            m_oWorker = new BackgroundWorker();
            m_oWorker.DoWork += new DoWorkEventHandler(m_oWorker_DoWork);
            m_oWorker.ProgressChanged += new ProgressChangedEventHandler(m_oWorker_ProgressChanged);
            m_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_oWorker_RunWorkerCompleted);
            m_oWorker.WorkerReportsProgress = true;
            m_oWorker.WorkerSupportsCancellation = true;

            log.Info("Program ready...");
        }

        void m_oWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //If it was cancelled midway
            if (e.Cancelled)
            {
                lblStatus.Text = "Task Cancelled.";
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error while performing background operation.";
            }
            else
            {
                lblStatus.Text = "Task Completed...";
            }
            btnStartAsyncOperation.Enabled = true;
            btnCancel.Enabled = false;
        }

        void m_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int overtimeProcesses = getSystemProcessListInfo();
            if (overtimeProcesses > 0)
            {

                lblStatus.Text = "Monitoring MSO processes.. found: " + overtimeProcesses;
                log.Debug("WORD|EXCEL active processes: " + overtimeProcesses);

                proccessMaxTimeSpan = getProcessTimeLimit();
                string proccessMaxTimeSpanString = convertTimeSpanToHumanRedable(proccessMaxTimeSpan);
                log.Debug("Process Timelimit: " + proccessMaxTimeSpanString);

            }
            else
            {
                lblStatus.Text = "Monitoring MSO processes.. ";
            }

        }

        private void btnStartAsyncOperation_Click(object sender, EventArgs e)
        {
            btnStartAsyncOperation.Enabled = false;
            btnCancel.Enabled = true;
            //Start the async operation here
            m_oWorker.RunWorkerAsync();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (m_oWorker.IsBusy)
            {
                //Stop/Cancel the async operation here
                m_oWorker.CancelAsync();
            }
            labelCurrentTime.Text = "Stopped: Last check time: " + DateTime.Now.ToString();
        }

        void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            while (true)
            {
                m_oWorker.ReportProgress(0);

                Thread.Sleep(1000);
                if (m_oWorker.CancellationPending)
                {
                    e.Cancel = true;
                    m_oWorker.ReportProgress(0);
                    return;
                }
            }
        }

        
        private int getSystemProcessListInfo()
        {

            labelCurrentTime.Text = "Current time: " + DateTime.Now.ToString();
            listViewProcess.Items.Clear();

            string currentTime = DateTime.Now.ToString();

            Process[] myProcs = Process.GetProcesses();
            var sorted = myProcs.OrderBy(p => p.ProcessName);

            proccessMaxTimeSpan = getProcessTimeLimit();
            string proccessMaxTimeSpanString = convertTimeSpanToHumanRedable(proccessMaxTimeSpan);

            int i = 0;
            foreach (Process process in myProcs)
            {
                if (process.ProcessName == "WINWORD" || process.ProcessName == "EXCEL")
                {
                    i++;

                    string processId = "";
                    string processName = "";
                    string processStartTimeString = "";
                    string processRunningTimeString = "";
                    string processOverTimeString = "";
                    string currentTimeString = currentTime;

                    ListViewItem item = new ListViewItem();

                    string logEntry = "";
                    // ProcessId
                    try
                    {
                        processId = process.Id.ToString();
                    }
                    catch (Exception ex)
                    {
                        log.Debug("ERROR getting Process ID: " + ex.StackTrace);
                        processId = "[ERROR]";
                    }
                    // -- 1 -- ProcessId
                    item.SubItems.Add(processId);
                    logEntry += "ID: " + processId;

                    try
                    {
                        processName = process.ProcessName;
                    }
                    catch (Exception ex)
                    {
                        log.Debug("ERROR getting ProcessName: " + ex.StackTrace);
                        processName = "[ERROR]";
                    }

                    // -- 2 -- ProcessName
                    item.SubItems.Add(processName);
                    logEntry += ";NAME: " + processName;

                    try
                    {
                        DateTime startTime = process.StartTime;
                        DateTime now = DateTime.Now;
                        TimeSpan processTime = now - startTime;

                        processStartTimeString = startTime.ToString();

                        processRunningTimeString = convertTimeSpanToHumanRedable(processTime);
                        logEntry += ";RunningTime: " + processRunningTimeString;

                        if (processTime > proccessMaxTimeSpan)
                        {
                            processOverTimeString = "[OVER TIMELIMIT!]";
                            logEntry += ";" + processOverTimeString;

                            if (checkBoxKillOverTimeProcesses.Checked)
                            {

                                process.Kill();
                                log.Info("Process with ID (" + processId + ") Killed! " + processOverTimeString);
                                continue;

                            }
                        }
                        


                    }
                    catch (Exception ex)
                    {
                        log.Debug("ERROR getting Process StartTime: " + ex.StackTrace);
                        processStartTimeString = "[ERROR]";

                    }

                    logEntry += ";StartTime: " + processStartTimeString;
                    log.Debug(logEntry);

                    // -- 3 -- processStartTimeString
                    item.SubItems.Add(processStartTimeString);
                    // -- 4 -- processRunningTimeString
                    item.SubItems.Add(processRunningTimeString);
                    // -- 5 -- processOverTimeString
                    item.SubItems.Add(processOverTimeString);
                    // -- 6 -- Current time
                    item.SubItems.Add(currentTimeString);

                    /*
                    try
                    {
                        textBoxVolumeName.Text += " Responding: " + process.Responding + ";\r\n";
                    }
                    catch (Exception ex)
                    {
                        textBoxVolumeName.Text += " Responding: [ERROR];\r\n";
                    }
                    try
                    {
                        textBoxVolumeName.Text += " SessionId: " + process.SessionId + ";\r\n";
                    }
                    catch (Exception ex)
                    {
                        textBoxVolumeName.Text += " SessionId: [ERROR];\r\n";
                    }
                    try
                    {
                        textBoxVolumeName.Text += " StartInfo.FileName: " + process.StartInfo.FileName + ";\r\n";
                    }
                    catch (Exception ex)
                    {
                        textBoxVolumeName.Text += " StartInfo.FileName: [ERROR];\r\n";
                    }

                    textBoxVolumeName.Text += "\r\n";
                    */


                    listViewProcess.Items.Add(item);
                }


            }

            foreach (ColumnHeader ch in listViewProcess.Columns)
            {
                ch.Width = -2;
            }

            labelCurrentTime.Text = "Program starttime: " + DateTime.Now.ToString();

            return i;

        }

        private static string convertTimeSpanToHumanRedable(TimeSpan time)
        {
            return time.Days + "d " + time.Hours + "h " + time.Minutes + "m " + time.Seconds + "s";
        }
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private TimeSpan proccessMaxTimeSpan;

        private static long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }


        public static string FormatByte(long bytes)
        {
            string result = String.Empty;

            if (bytes < 1024)
                result = String.Format("{0} B", bytes);
            else if (bytes < 1024 * 1024)
                result = String.Format("{0:0.00} KB", (float)bytes / (1024));
            else if (bytes < 1024 * 1024 * 1024)
                result = String.Format("{0:0.00} MB", (float)bytes / (1024 * 1024));
            else
                result = String.Format("{0:0.00} GB", (float)bytes / (1024 * 1024 * 1024));

            return result;
        }
        private void MSOProcessMonitor_Load(object sender, EventArgs e)
        {
            listViewProcess.View = View.Details;
            listViewProcess.LabelEdit = false;
            listViewProcess.AllowColumnReorder = true;
            listViewProcess.FullRowSelect = true;
            listViewProcess.CheckBoxes = false;
            listViewProcess.GridLines = true;
            listViewProcess.Sorting = SortOrder.Ascending;

            listViewProcess.Items.Clear();

            foreach (ColumnHeader ch in listViewProcess.Columns)
            {
                ch.Width = -2;
            }

            labelCurrentTime.Text = "Program starttime: " + DateTime.Now.ToString();
        }

        private TimeSpan getProcessTimeLimit()
        {
            double proccessMaxTime = 300;
            TimeSpan proccessMaxTimeSpan = TimeSpan.FromSeconds(proccessMaxTime);

            try
            {
                proccessMaxTime = Convert.ToDouble(textBoxProcessTimelimit.Text);
                proccessMaxTimeSpan = TimeSpan.FromSeconds(proccessMaxTime);

                labelProcessTimeLimitText.Text = "Process max time: " + convertTimeSpanToHumanRedable(proccessMaxTimeSpan);
                labelProcessTimeLimitText.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                //labelProcessTimeLimitText.Font.Bold = true;
                labelProcessTimeLimitText.ForeColor = Color.Red;
                labelProcessTimeLimitText.Text = "ERROR: NOT NUMERIC! USING DEFAULT: " + proccessMaxTime;

            }


            return proccessMaxTimeSpan;

        }
        private void textBoxProcessTimelimit_TextChanged(object sender, EventArgs e)
        {
            getProcessTimeLimit();
        }
    }
}
