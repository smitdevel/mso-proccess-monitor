﻿namespace MSOProccessMonitor
{
    partial class MSOProcessMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.checkBoxKillOverTimeProcesses = new System.Windows.Forms.CheckBox();
            this.btnStartAsyncOperation = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listViewProcess = new System.Windows.Forms.ListView();
            this.columnProcessId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnProcessName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnStartTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnProcessRunningTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxProcessTimelimit = new System.Windows.Forms.TextBox();
            this.labelProcessTimeLimitText = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.columnOverTimelimit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCurrentTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelCurrentTime = new System.Windows.Forms.Label();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblStatus);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listViewProcess);
            this.splitContainer1.Size = new System.Drawing.Size(1110, 772);
            this.splitContainer1.SplitterDistance = 130;
            this.splitContainer1.TabIndex = 0;
            // 
            // checkBoxKillOverTimeProcesses
            // 
            this.checkBoxKillOverTimeProcesses.AutoSize = true;
            this.checkBoxKillOverTimeProcesses.Checked = true;
            this.checkBoxKillOverTimeProcesses.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxKillOverTimeProcesses.Location = new System.Drawing.Point(15, 20);
            this.checkBoxKillOverTimeProcesses.Name = "checkBoxKillOverTimeProcesses";
            this.checkBoxKillOverTimeProcesses.Size = new System.Drawing.Size(137, 17);
            this.checkBoxKillOverTimeProcesses.TabIndex = 0;
            this.checkBoxKillOverTimeProcesses.Text = "Kill overlimit processes..";
            this.checkBoxKillOverTimeProcesses.UseVisualStyleBackColor = true;
            // 
            // btnStartAsyncOperation
            // 
            this.btnStartAsyncOperation.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStartAsyncOperation.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnStartAsyncOperation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartAsyncOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.btnStartAsyncOperation.Location = new System.Drawing.Point(6, 19);
            this.btnStartAsyncOperation.Name = "btnStartAsyncOperation";
            this.btnStartAsyncOperation.Size = new System.Drawing.Size(187, 23);
            this.btnStartAsyncOperation.TabIndex = 1;
            this.btnStartAsyncOperation.Text = "Start monitoring...";
            this.btnStartAsyncOperation.UseVisualStyleBackColor = true;
            this.btnStartAsyncOperation.Click += new System.EventHandler(this.btnStartAsyncOperation_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.btnCancel.Location = new System.Drawing.Point(6, 46);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(187, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "seconds";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStartAsyncOperation);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 74);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Start monitoring...";
            // 
            // listViewProcess
            // 
            this.listViewProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnProcessId,
            this.columnProcessName,
            this.columnStartTime,
            this.columnProcessRunningTime,
            this.columnOverTimelimit,
            this.columnCurrentTime});
            this.listViewProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewProcess.FullRowSelect = true;
            this.listViewProcess.GridLines = true;
            this.listViewProcess.HoverSelection = true;
            this.listViewProcess.Location = new System.Drawing.Point(0, 0);
            this.listViewProcess.Name = "listViewProcess";
            this.listViewProcess.ShowItemToolTips = true;
            this.listViewProcess.Size = new System.Drawing.Size(1110, 638);
            this.listViewProcess.TabIndex = 0;
            this.listViewProcess.UseCompatibleStateImageBehavior = false;
            this.listViewProcess.View = System.Windows.Forms.View.Details;
            // 
            // columnProcessId
            // 
            this.columnProcessId.Text = "Process ID";
            this.columnProcessId.Width = 80;
            // 
            // columnProcessName
            // 
            this.columnProcessName.Text = "Process name";
            this.columnProcessName.Width = 200;
            // 
            // columnStartTime
            // 
            this.columnStartTime.Text = "Process starttime";
            this.columnStartTime.Width = 100;
            // 
            // columnProcessRunningTime
            // 
            this.columnProcessRunningTime.Text = "Running time";
            this.columnProcessRunningTime.Width = 100;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelCurrentTime);
            this.groupBox2.Controls.Add(this.labelProcessTimeLimitText);
            this.groupBox2.Controls.Add(this.textBoxProcessTimelimit);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(219, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(580, 74);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Process Timelimit";
            // 
            // textBoxProcessTimelimit
            // 
            this.textBoxProcessTimelimit.Location = new System.Drawing.Point(0, 20);
            this.textBoxProcessTimelimit.Name = "textBoxProcessTimelimit";
            this.textBoxProcessTimelimit.Size = new System.Drawing.Size(100, 20);
            this.textBoxProcessTimelimit.TabIndex = 4;
            this.textBoxProcessTimelimit.Text = "300";
            this.textBoxProcessTimelimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxProcessTimelimit.TextChanged += new System.EventHandler(this.textBoxProcessTimelimit_TextChanged);
            // 
            // labelProcessTimeLimitText
            // 
            this.labelProcessTimeLimitText.AutoSize = true;
            this.labelProcessTimeLimitText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.labelProcessTimeLimitText.ForeColor = System.Drawing.Color.Black;
            this.labelProcessTimeLimitText.Location = new System.Drawing.Point(7, 47);
            this.labelProcessTimeLimitText.Name = "labelProcessTimeLimitText";
            this.labelProcessTimeLimitText.Size = new System.Drawing.Size(113, 13);
            this.labelProcessTimeLimitText.TabIndex = 5;
            this.labelProcessTimeLimitText.Text = "Process max time: ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxKillOverTimeProcesses);
            this.groupBox3.Location = new System.Drawing.Point(805, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 74);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Actions";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(12, 90);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(184, 23);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Monitoring stopped...";
            // 
            // columnOverTimelimit
            // 
            this.columnOverTimelimit.Text = "Over timelimit";
            this.columnOverTimelimit.Width = 100;
            // 
            // columnCurrentTime
            // 
            this.columnCurrentTime.Text = "Current time";
            this.columnCurrentTime.Width = 100;
            // 
            // labelCurrentTime
            // 
            this.labelCurrentTime.AutoSize = true;
            this.labelCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.labelCurrentTime.Location = new System.Drawing.Point(196, 26);
            this.labelCurrentTime.Name = "labelCurrentTime";
            this.labelCurrentTime.Size = new System.Drawing.Size(83, 13);
            this.labelCurrentTime.TabIndex = 6;
            this.labelCurrentTime.Text = "CurrentTime: ";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 20;
            // 
            // MSOProcessMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 772);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MSOProcessMonitor";
            this.Text = "MSO process monitor";
            this.Load += new System.EventHandler(this.MSOProcessMonitor_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnStartAsyncOperation;
        private System.Windows.Forms.CheckBox checkBoxKillOverTimeProcesses;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listViewProcess;
        private System.Windows.Forms.ColumnHeader columnProcessId;
        private System.Windows.Forms.ColumnHeader columnProcessName;
        private System.Windows.Forms.ColumnHeader columnStartTime;
        private System.Windows.Forms.ColumnHeader columnProcessRunningTime;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelProcessTimeLimitText;
        private System.Windows.Forms.TextBox textBoxProcessTimelimit;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ColumnHeader columnOverTimelimit;
        private System.Windows.Forms.ColumnHeader columnCurrentTime;
        private System.Windows.Forms.Label labelCurrentTime;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}

